package com.kavya.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kavya.springdemo.entity.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {
	
	//need to inject sessionFactory bean from spring-mvc-crud-demo-servlet.xml
	
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	@Override
	public List<Customer> getCustomers() {
		//get the current hibernate session
		Session currentSession=sessionFactory.getCurrentSession();
		
		//create a query
		Query<Customer> query=currentSession.createQuery("from Customer",Customer.class);
		
		//execute a query and get result list
		List<Customer> customers=query.getResultList();
		//return result
		return customers;
	}

}
