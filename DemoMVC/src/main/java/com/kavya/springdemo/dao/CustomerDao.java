package com.kavya.springdemo.dao;

import java.util.List;

import com.kavya.springdemo.entity.Customer;

public interface CustomerDao {
	
	public List<Customer> getCustomers();

}
