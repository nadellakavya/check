package com.kavya.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kavya.springdemo.dao.CustomerDao;
import com.kavya.springdemo.entity.Customer;

@Controller
@RequestMapping("/customer")
public class CustomerController {
  //Inject DAO in controller
	@Autowired
	private CustomerDao customerDao;
	@RequestMapping("/get")
	public String listCustomers(Model theModel) {
		
		//get customers from Dao
		
		List<Customer> customers=customerDao.getCustomers();
		
		//add to the model
		
		theModel.addAttribute("customers",customers);
		return "get-customers";
	}

}
